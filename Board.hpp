/******************************************************************************
 * Filename: Board.hpp
 * Author: Jens Bodal
 * Date: February 15, 2015
 * Description: Header defintion for a tictactoe game board 
 *****************************************************************************/

#ifndef BOARD_HPP
#define BOARD_HPP

enum GameStatus {X_WON, O_WON, DRAW, IN_PROGRESS};

class Board {
    private:
        static const int ROWS_COLS = 3;
        char gameBoard[ROWS_COLS][ROWS_COLS];
    public:
        static const char PIECE1 = 'X';
        static const char PIECE2 = 'O';
        Board();
        bool makeMove(int, int, char);
        GameStatus gameState();
        void print();
};

#endif
