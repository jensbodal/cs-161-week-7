/******************************************************************************
 * Filename: ShopCart.cpp
 * Author: Jens Bodal
 * Date: February 21, 2015
 * Description: This program will create a ShoppingCart then prompt the user
 * with a menu which will allow them to add an item to the cart with a varying
 * quantity of the item, print out the cart's contents, and to show the total 
 * price of all of the items in the cart.
 * Input: items to add to cart
 * Output: contents of cart, total price of all items in the cart
 *****************************************************************************/

#include "Item.hpp"
#include "ShopCart.hpp"
#include <iomanip>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

int main() { 
    ShopCart cart;
    // controls flow of entire program
    bool running = true;
    // allows checking for valid item before adding to cart
    bool waitForItem = false;
    std::string itemName;
    double price;
    int quantity;

    while (running) {
        int selection = 0;
        std::cout << std::endl;
        std::cout << "1. Add item\n";
        std::cout << "2. List contents\n";
        std::cout << "3. Total price\n";
        std::cout << "4. Quit\n";
        std::cout << "Selection: ";
        std::cin >> selection;
        switch (selection) {
            case 1:
                waitForItem = true;
                break;
            case 2:
                cart.listContents();
                break;
            case 3:
                std::cout << std::setprecision(2) << std::fixed;
                std::cout << "\nThe total cost for all items in the shopping";
                std::cout << " cart is: $" << cart.totalCost();
                std::cout << std::endl << std::endl;
                break;
            case 4:
                running = false;
                break;
            default:
                break;
        }
        // we need to wait for a valid item to be created before continuing
        while (waitForItem) {
            bool askPrice = true;
            bool askQuantity = true;
            // ensure cin is cleared from prior selections
            std::cin.ignore(std::numeric_limits<int>::max(),'\n');
            std::cout << "Item: ";
            std::getline(std::cin, itemName);
            
            // sub routine for obtaining a valid price input
            while (askPrice) {
                std::cout << "Price: ";
                std::cin >> price;
                if (std::cin.fail() || std::cin.peek() != '\n') {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<int>::max(),'\n');
                    std::cout << "\nPrice is invalid, try again\n";
                }
                else {
                    askPrice = false;
                }
            }

            // sub routine for obtaining a valid quantity for the item
            while (askQuantity) {
                std::cout << "Quantity: ";
                std::cin >> quantity;
                if (std::cin.fail() || std::cin.peek() != '\n') {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<int>::max(),'\n');
                    std::cout << "\nQuantity is invalid, try again\n";
                }
                else {
                    askQuantity = false;
                }

            }
            // add item to cart with specified name, price, and quantity
            cart.addItem(Item(itemName, price, quantity));
            std::cout << std::endl;
            waitForItem = false;
        }
    }
    return 0;
}

/******************************************************************************
 * Description: Adds an item to the shopping cart
 * Parameters: Item object to add to cart
 *****************************************************************************/
void ShopCart::addItem(Item item) {
    items.push_back(item);
}

/******************************************************************************
 * Description: Outputs the contents of all items in the shopping cart including
 * their price information and quantity of item in the cart.  This will not
 * aggregate items that have the same name that were entered at different times
 * Parameters: none
 *****************************************************************************/
void ShopCart::listContents() {
    std::cout << "\nContents of shopping cart\n";
    for (int i = 0; i < items.size(); i++) {
        std::cout << std::fixed << std::setprecision(2);
        std::cout << "\tItem: " << items[i].getName();
        std::cout << "\n\t    Price: $" << items[i].getPrice();
        std::cout << "\n\t    Quantity: " << items[i].getQuantity();
        std::cout << std::endl;
    }
}

/******************************************************************************
 * Description: Calculates the total cost of all items in the shopping cart 
 * Parameters: none
 *****************************************************************************/
double ShopCart::totalCost() {
    int total = 0;
    // loop through all items in cart
    for (int i = 0; i < items.size(); i++) {
        // get the price of each item
        double price = items[i].getPrice();
        // get the quantity of each item
        int quantity = items[i].getQuantity();
        // increment the total by the total value of each item by its quantity
        total += (price*quantity);
    }
    return total;
}
