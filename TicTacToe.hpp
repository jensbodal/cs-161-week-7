/******************************************************************************
 * Filename: TicTacToe.hpp
 * Author: Jens Bodal
 * Date: February 17, 2015
 * Description: Header definition of a TicTacToe game
 *****************************************************************************/

#ifndef TIC_TAC_TOE_HPP
#define TIC_TAC_TOE_HPP

#include "Board.hpp"

class TicTacToe {
    private:
        Board board;
        char playerTurn;
    public:
        TicTacToe(char player);
        void play();
};

#endif
