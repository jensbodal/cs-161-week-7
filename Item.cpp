/******************************************************************************
 * Filename: Item.cpp
 * Author: Jens Bodal
 * Date: February 21, 2015
 * Description: The Item class has the properties of an item to be placed inside
 * of a shopping cart.  It tracks the item's name, price, and quantity of that
 * item.
 *****************************************************************************/

#include "Item.hpp"
#include <string>

std::string DEFAULT_NAME;
double DEFAULT_PRICE;
int DEFAULT_QUANTITY;

/******************************************************************************
 * Description: Creates Item object based on default constant constructors
 * Parameters: none
 *****************************************************************************/
Item::Item() {
    setName(DEFAULT_NAME);
    setPrice(DEFAULT_PRICE);
    setQuantity(DEFAULT_QUANTITY);
}

/******************************************************************************
 * Description: Creates Item object based on provided parameters
 * Parameters: item name, price, and quantity
 *****************************************************************************/
Item::Item(std::string iName, double iPrice, int iQuantity) {
    setName(iName);
    setPrice(iPrice);
    setQuantity(iQuantity);
}

void Item::setName(std::string iName) {
    name = iName;
}

std::string Item::getName() {
    return name;
}

void Item::setPrice(double iPrice) {
    price = iPrice;
}

double Item::getPrice() {
    return price;
}

void Item::setQuantity(int iQuantity) {
    quantity = iQuantity;
}

int Item::getQuantity() {
    return quantity;
}
