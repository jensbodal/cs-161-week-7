/******************************************************************************
 * Filename: TicTacToe.cpp
 * Author: Jens Bodal
 * Date: February 17, 2015
 * Description: This program allows two people to play a game of TicTacToe
 * Input: Select which player goes first
 * Output: Output of game and any error messages or status messages
 *****************************************************************************/

#include "Board.hpp"
#include "TicTacToe.hpp"
#include <limits>
#include <iostream>


/******************************************************************************
 * Description: Will output the information on who won the game.  If no one wins
 * the game or it is not a draw, then nothing will be output
 * Parameters: The GameStatus of the game to evaluate
 *****************************************************************************/
void printGameStatus(GameStatus status);

int main() {
    std::cout << std::endl;
    bool initializing = true;
    char firstPlayer;
    
    /**
      * Allows the user to select which player goes first.  The default pieces
      * are 'X' and 'O', and are specified in the Board header file. The default
      * piece expects a single capital letter, as such any character input will
      * be cast to uppercase
      */
    while (initializing) {
        std::cout << "Who goes first? Enter " << Board::PIECE1 << " or ";
        std::cout << Board::PIECE2 << ": ";
        std::cin >> firstPlayer;
        firstPlayer = toupper(firstPlayer);
        if (firstPlayer == Board::PIECE1 || firstPlayer == Board::PIECE2) {
            initializing = false;
        }
        else {
            std::cout << "You must choose between " << Board::PIECE1 << " or ";
            std::cout << Board::PIECE2;
            std::cout << std::endl;
        }
    }
    
    TicTacToe game(firstPlayer);
    game.play();
    return 0; 
}

/******************************************************************************
 * Description: Creates a TicTacToe game with the specified character being used
 * as determining which player goes first.  Creates a default Board
 * Parameters: character input for who goes first
 *****************************************************************************/
TicTacToe::TicTacToe(char player) {
    board = Board();
    playerTurn = player;
}

/******************************************************************************
 * Description: Starts the game of Tic Tac Toe
 * Parameters: none
 *****************************************************************************/
void TicTacToe::play() {
    // Set status to IN_PROGRESS for use in loop knowing that game is active
    GameStatus status = IN_PROGRESS;
    // Reference const piece variables in board class to set player1 & player2
    char piece1 = Board::PIECE1;
    char piece2 = Board::PIECE2;

    char player1 = playerTurn;
    char player2 = playerTurn == piece1 ? piece2 : piece1;
    int row;
    int col;
    
    // output board at start of game
    board.print();
    // output who is going first at start of game
    std::cout << "\n*** " << playerTurn << " goes first ***\n\n";

    while (status == IN_PROGRESS) {
        // Specify whose turn it is and ask them for a row and column
        std::cout << "Player " << playerTurn << "'s turn\n";
        std::cout << "Enter the coordinates of your move: \n";
        std::cout << "    row: ";
        std::cin >> row;
        std::cout << "    col: ";
        std::cin >> col;

        // Ensure integers were received for row and column, reprompt otherwise
        if (std::cin.fail() || std::cin.peek() != '\n') {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<char>::max(), '\n');
            std::cout << "\n\nINVALID MOVE - TRY AGAIN\n";
        }

        else if (board.makeMove(row, col, playerTurn)) {
            // switch players if move was successful
            playerTurn = playerTurn == player1 ? player2 : player1;
        }
         
        std::cout << std::endl;
        // set the status of our game by evaluating the game state
        status = board.gameState();
        // output board before looping again or ending game
        board.print();
        // If there is a winner or its a draw this will print, otherwise nothing
        printGameStatus(status);
    }
}

void printGameStatus(GameStatus status) {
    switch (status) {
        case X_WON:
            std::cout << "X won!!!!\n\n";
            break;
        case O_WON:
            std::cout << "O won!!!!\n\n";
            break;
        case DRAW:
            std::cout << "The game ended in a Draw\n\n";
            break;
        default:
            break;
    }
}
