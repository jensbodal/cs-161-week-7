/******************************************************************************
 * Filename: Item.hpp
 * Author: Jens Bodal
 * Date: February 21, 2015
 * Description: Definition header file for an Item object to be used in a
 * shopping cart
 *****************************************************************************/

#ifndef ITEM_HPP
#define ITEM_HPP

#include <string>

class Item {
    private:
        std::string name;
        double price;
        int quantity;
    public:
        Item();
        Item(std::string iName, double iPrice, int iQuantity);
        void setName(std::string iName);
        std::string getName();
        void setPrice(double iPrice);
        double getPrice();
        void setQuantity(int iQuantity);
        int getQuantity();
};

#endif
