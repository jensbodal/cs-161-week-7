/******************************************************************************
 * Filename: ShopCart.hpp
 * Author: Jens Bodal
 * Date: February 21, 2015
 * Description: Definition header file for a ShoppingCart object
 *****************************************************************************/

#ifndef SHOP_CART_HPP
#define SHOP_CART_HPP

#include "Item.hpp"
#include <vector>

class ShopCart {
    private:
        std::vector<Item> items;
    public:
        void addItem(Item item);
        void listContents();
        double totalCost();
};

#endif
