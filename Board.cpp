/******************************************************************************
 * Filename: Board.cpp
 * Author: Jens Bodal
 * Date: February 15, 2015
 * Description: Defines a Board object for a standard Tic Tac Toe game.  There
 * must be equal rows and column, and this constant is defined in the header
 * file.
 * Input: none
 * Output: error messages if improper player is used and print() method for
 * printing the current board and players that have been placed
 *****************************************************************************/

#include "Board.hpp"
#include <iomanip>
#include <iostream>
#include <string>

const char EMPTY = '.';

/******************************************************************************
 * Description: Assigns an integer value to the player to be used in calculating
 * whether a player has won. PIECE1 is 1, PIECE2 is -1, and 0 otherwise.
 * Parameters: the gamePlayer to convert to an integer value
 *****************************************************************************/
int playerToValue(char const &player);

/******************************************************************************
 * Description: Creates an empty board with default rows and columns and "fills"
 * each space with the EMPTY char constant
 * Parameters: none
 *****************************************************************************/
Board::Board() {
    for (int row = 0; row < ROWS_COLS; row++) {
        for (int col = 0; col < ROWS_COLS; col++) {
            gameBoard[row][col] = EMPTY;
        }
    }
}

/******************************************************************************
 * Description: Allows the player to move onto a space, if the space is occupied
 * or the player doesn't exist, this will return false indicating no move was
 * made.  If the move is successful, will return true.
 * Parameters: the row and column to move the player two, and the player to
 * place in the space
 * Returns: true if move was made successfully, false otherwise
 *****************************************************************************/
bool Board::makeMove(int row, int col, char player) {
    if (player != PIECE1 && player != PIECE2) {
        std::cout << "Player must be " << PIECE1 << " or " << PIECE2 << "\n";
        return false;
    }
    if (gameBoard[row][col] == EMPTY) {
        gameBoard[row][col] = player;
        return true;
    }
    return false;
}

/******************************************************************************
 * Description: Obtains the state of the game.  The game can be either in
 * progress, a draw, or player 1 or player 2 has won
 * Parameters: none
 *****************************************************************************/
GameStatus Board::gameState() {
    /**
      * The general idea for obtaining whether or not a player has won is to
      * assign each player an integer value.  Player1 is assigned a positive 1
      * for each space they occupy, whereas Player2 is assigned a negative 1.
      * If the space is empty it equals 0.  If the sum of the values in a row,
      * column, or diagonal is equal to the number of Rows/Columns in the board
      * then Player1 has won; if it is equal to the negative value of the
      * Rows/Columns, then Player2 has won.
      *
      * If any of the spaces contain a 0, then the game is still in progress.
      * If none of the spaces contain a 0, and neither player has won, then
      * the game must be a draw.
      */

    // the total sum needed in a row, column, or diagonal for x to win
    const int xWinSum = ROWS_COLS;
    // the total sum needed in a row, column, or diagonal for o to win
    const int oWinSum = ROWS_COLS * - 1;
    // creating row/column variables to make for loops more readable
    const int rows = ROWS_COLS;
    const int columns = ROWS_COLS;
    
    // if this remains false, and no one has won, then game is a draw
    bool hasZeroes = false;
    // initialize four potential ways of winning to 0
    int sumDiagRight = 0;
    int sumDiagLeft = 0; 
    int sumRow = 0;
    int sumCol = 0;
    // this marker indicates if the coordinates are within the left diagonal
    int diagLeftMarker = ROWS_COLS - 1;

    // Loop through rows
    for (int row = 0; row < rows; row++) {
        // reset current sum of rows and columns for each loop through rows
        sumRow = 0;
        sumCol = 0;
        
        // Loop through columns
        for (int col = 0; col < columns; col++) {
            
            /**
              * Check columns for a winner as we iterate columns
              * We use the fact that rows == columns to essentially obtain
              * the value of the column with the same index of the current row
              * by referencing (col,row) instead of (row,col)
              */
            sumCol += playerToValue(gameBoard[col][row]);
                
            // check if value is in diagonal from top left to bottom right
            if (row == col) {
                sumDiagRight += playerToValue(gameBoard[row][col]);
            }

            // check if value is in diagonal from top right to bottom left
            if (col - row == diagLeftMarker) {
                sumDiagLeft += playerToValue(gameBoard[row][col]);
            }
        
            // sum values for current row as we loop through columns
            sumRow += playerToValue(gameBoard[row][col]);

            // if any of the sums equal the sum needed for x to win, then x wins
            if (
                    xWinSum == sumRow || 
                    xWinSum == sumCol ||
                    xWinSum == sumDiagRight ||
                    xWinSum == sumDiagLeft) {
                return X_WON;
            }

            // if any of the sums equal the sum needed for o to win, then o wins
            if (
                    oWinSum == sumRow || 
                    oWinSum == sumCol ||
                    oWinSum == sumDiagRight ||
                    oWinSum == sumDiagLeft) {
                return O_WON;
            }
            
            // Check if board contains ANY zeroes
            if (!hasZeroes) {
                if (playerToValue(gameBoard[row][col]) == 0) {
                    hasZeroes = true;
                }
            }
        } // end loop through columns
        diagLeftMarker-=2;
    } // end loop through rows
    
    // If any of the game spaces are 0, game is still in progress
    if (hasZeroes) {
        return IN_PROGRESS;
    }
    else {
        return DRAW;
    }
} // end gameState()

/******************************************************************************
 * Description: Prints out the current board and the player moves
 * Parameters: none
 *****************************************************************************/
void Board::print() {
    int row = ROWS_COLS;
    int col = ROWS_COLS;
    // width of each space as used by setw
    int width = 3;
    
    // create empty space in top left of board
    std::cout << std::endl << std::setw(width) << "";

    // print out column headers
    for (int i = 0; i < col; i++) {
        std::cout << std::setw(width) << i;
    }
    std::cout << std::endl << std::endl;

    // print out row headers and game board values
    for (int i = 0; i < row; i++) {
        // prints row header
        std::cout << std::setw(width) << i;
        for (int j = 0; j < col; j++) {
            // prints value of space
            std::cout << std::right << std::setw(width) << gameBoard[i][j];
        }
    std::cout << std::endl << std::endl;

    }

    std::cout << std::endl;
}

int playerToValue(char const &player) {
    switch (player){
        case Board::PIECE1:
            return 1;
        case Board::PIECE2:
            return -1;
        default:
            return 0;
    }
}
