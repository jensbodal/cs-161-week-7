/******************************************************************************
 * Filename: caesar.cpp
 * Author: Jens Bodal
 * Date: February 21, 2015
 * Description: This program will encipher the user's input using the caesar
 * cipher, which uses an offset to move each character n number of spaces down
 * in the alphabet, Zz looping back to Aa
 * Input: user message to encode
 * Output: the encoded message
 *****************************************************************************/

#include <cstring> // c_str
#include <iostream>
#include <string>

const int MAX_MESSAGE_LENGTH = 100;

/******************************************************************************
 * Description: Takes an input message and applies the caesar cipher then
 * returns the encrypted message
 * Parameters: the message and the offset to apply for the cipher
 *****************************************************************************/
void encipher(char message[], int offset);

int main() {
    int offset;
    std::string inputString;
    std::cout << "Input message: ";
    std::getline(std::cin, inputString);
    std::cout << "Caesar offset: ";
    std::cin >> offset;
    
    // get user input as string as we can obtain varyling length input
    char inputMessage[MAX_MESSAGE_LENGTH];
    // copy input into a cstring as it is required for encipher method parameter
    std::strcpy(inputMessage, inputString.c_str());
    
    encipher(inputMessage, offset);
    return 0;
}

void encipher(char message[], int offset) {
    int messageLength = strlen(message); 
    char outputMessage[MAX_MESSAGE_LENGTH];
    // copy current message to output return message to ensure proper size
    std::strcpy(outputMessage, message);
    for (int i = 0; i < messageLength; i++) {
        char c = message[i];
        /**
          * Need to know the the point in which we start looping backwards
          * through alphabet instead of forwards
          */
        char lboundCap = 'Z' - offset;
        char lboundLow = 'z' - offset;
        /**
          * if the current character plus the offset would put it past either
          * lowercase or capital Z, then instead subtract the offset from the
          * number of letters in the alphabet (26) and set the current character
          * equal to this many places below.  
          * For example if offset is 3, and current character is y, the ascii
          * value for y is 121 -- from here, incrementing the character per
          * the ciper would result in: +1 = 'z', +2 = 'a', and +3 = 'b'.
          * So using the above methodology, 121 - (26-3) = 121-23 = 98 which is
          * the ascii value for 'b'.
          */
        if ((c > lboundCap && c <= 'Z') || (c > lboundLow && c <= 'z')) {
            c -= (26 - offset);
        }
        /** 
          * set the character equal to the ascii character 'offset' number
          * of characters above the current character
          */
        else if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
            c += offset;
        }
        // start building output message
        outputMessage[i] = c;
    }
    std::cout << outputMessage;
    std::cout << std::endl;    
}
